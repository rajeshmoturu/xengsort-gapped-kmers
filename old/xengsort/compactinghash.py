"""
Definition of a Compacting Hash Table
"""

from math import ceil
from collections import namedtuple

import numpy as np
from numba import njit, int64, uint8

from .dnaencode import generate_revcomp_and_canonical_code
from .generalhash import generate_get_page_and_get_fingerprint, print_info


CompactingHash = namedtuple("CompactingHash",
    ["hashtype",
     "pagebits",
     "fprbits",
     "hashfunc",
     "sbits",
     "srate",

     "hashtable",
     "pagesizes",
     "cumsizes",

     "get_page",
     "get_fingerprint",
     "find_or_set_genome",
     "find_pagestart",
     "get_item",
     "set_item",
     "build_cumsizes",
     "count_valid_qgrams_per_page",
     "hash_qgrams",
     "compactify",
     "get_occupancy",
    ])


def generate_compacting_hash(q, pagebits, fprbits, hashfunc, 
        sbits=32, srate=64, pagesizes=None):
    """
    Allocate an array and compile access methods for a hash table.
    Return a SingleHash namedtuple instance.
    
    Parameters:
    q:        q-gram length; all fingerprints are based on q-grams (2q bits)
    pagebits: number of bits for indexing a hash table page;  pagebits < 2*q
    fprbits:  number of bits for fingerprints;  ideally pagebits + fprbits == 2*q
    hashfunc: name of hash function (string), see generalhash.py.
    sbits:    number of bits for cumulative counters (32 or 64).
    srate:    sampling rate, counter for each sampling-th page is stored.
    pagesizes: None or array with individual page sizes.
    """

    qbits = 2*q
    if fprbits < 0:
        fprbits = qbits - pagebits
    if fprbits + pagebits > 2*q or pagebits <= 0:
        raise ValueError("must have 0 < pagebits <= pagebits+fprbits <= 2*q")
    if fprbits > 62:
        raise ValueError("must have fprbits <= 62")
    slotbits = fprbits + 2
    assert slotbits <= 64

    if pagesizes is None:
        # we don't know page sizes yet. Initialize them.
        pagesizes = np.zeros(2**pagebits, dtype=np.uint8)
        nslots = tablebits = tablewords = 0
    else:
        # we have page sizes, initialize a table.
        nslots = np.sum(pagesizes)
        tablebits = nslots * slotbits
        tablewords = int(ceil(tablebits / 64))
    hashtable = np.zeros(tablewords, dtype=np.int64)

    nsamples = int(ceil(2**pagebits / srate))
    if sbits == 32:
        cumsizes = np.zeros(nsamples, dtype=np.uint32)
        maxcs = 2**32 - 1
    elif sbits == 64:
        cumsizes = np.zeros(nsamples, dtype=np.int64)
        maxcs = 2**63 - 1
    else:
        raise ValueError("samplebits ('sbits') must be 32 or 64")


    # print statistics
    mem1 = 2**pagebits + cumsizes.itemsize * nsamples  # bytes for counters
    mem2 = tablewords * 8  # bytes for table
    print("CompactingHash:")
    print("    bits for page address:", pagebits)
    print("    bits per slot (fingerprint + genome):", slotbits)
    print("    counters: {} bytes + {} {}: {:.6f} GiB".format(
        2**pagebits, nsamples, cumsizes.dtype, mem1/10**9))
    print("    hash table: {:.6f} GiB".format(mem2/10**9))
    if 0 < mem2 < mem1:
        print("WARNING: hash table uses less memory than counters, decrease pagebits!")
    print()

    # define masks, get_page(code) and get_fingerprint(code) functions
    fprmask = 2**fprbits - 1
    (get_page, get_fingerprint) = generate_get_page_and_get_fingerprint(
        hashfunc, q, pagebits, fprbits)


    @njit(locals=dict(code=int64, setgenome=int64,
            page=int64, psize=int64, pstart=int64, fpr=int64, slot=int64,
            x=int64, g=int64, f=int64, newgenome=int64),
          nogil=True, fastmath=True)
    def find_or_set_genome(table, pss, css, code, setgenome=0):
        """
        Compute the page number for given 'code'
        find it in the 'table' using pagesizes 'pss' and cumsizes 'css'.
        If the code is found in the table:
            If 'setgenome' == 0, return its current genome value > 0.
            If 'setgenome' > 0, OR (|) it with its current genome value.
            Return the ORed genome value > 0.
        If the code is not found in the table:
            If setgenome == 0, return 0 (not found).
            If setgenome > 0:
                If the page is not full, insert it with the given 'setgenome',
                    and return 'setgenome'.
                If the page is full, return -1 (overflow).

        In summary, return current (updated) genome, which is 
        0 if not found, -1 if trying to extend an already full page.
        """
        page = get_page(code)
        psize = pss[page]
        if psize == 0:
            if setgenome == 0: return 0  # not found
            return -1  # overflow on empty page
        pstart = find_pagestart(page, pss, css)
        fpr = get_fingerprint(code)
        for slot in range(pstart, pstart+psize):
            x = get_item(table, slot)
            g = (x >> fprbits) & 3
            if g == 0:
                # code does not exist, empty slot
                if setgenome == 0:  return 0
                set_item(table, slot, fpr, setgenome)
                return setgenome
            # slot occupied because g != 0
            f = x & fprmask
            if f == fpr:
                # found
                if setgenome == 0:  return g
                newgenome = setgenome | g
                if newgenome == g:  return g
                set_item(table, slot, fpr, newgenome)
                return newgenome
            # slot occupled, but different fingerprint; do nothing
        # we fall out of the 'for' loop, not found, page full.
        if setgenome == 0: return 0  # not found
        return -1  # overflow


    @njit(locals=dict(page=int64, i=int64, j=int64, pstart=int64, k=int64),
          nogil=True, fastmath=True)
    def find_pagestart(page, pss, css):
        i = page // srate
        j = page % srate
        pstart = css[i]
        for k in range(i*srate, i*srate+j):
            pstart += pss[k]
        return pstart


    # Extract the item on a given page in a given slot
    @njit(locals=dict(x=int64, mask1=int64, a=int64, b=int64, b1=int64),
          nogil=True, fastmath=True)
    def get_item(table, slot):
        startbit = slot * slotbits
        a = startbit // 64
        b = startbit & 63
        if b + slotbits <= 64:  # get slotbits bits
            # bits are contained in a single int64
            x = table[a] >> b
        else:
            # bits are distributed over two int64s, 
            # leftmost b1 = 64-b in table[a], rightmost b2 = slotbits-b1 in table[a+1]
            b1 = 64 - b
            mask1 = 2**b1 - 1
            x = (table[a] >> b) & mask1 
            x |= (table[a+1] << b1)
        return x


    @njit(locals=dict(f=int64, g=int64,
              v=int64, v1=int64, startbit=int64, a=int64, b=int64, 
              b1=int64, b2=int64, mask1=int64, mask2=int64),
          nogil=True, fastmath=True)
    def set_item(table, slot, f, g):
        # set fingerprint and genome bits to f and g, respectively
        v = (f & fprmask) | ((g&3) << fprbits)  # slotbits
        startbit = slot * slotbits
        a = startbit // 64
        b = startbit & 63
        if b + slotbits <= 64:
            # bits are contained in a single int64
            mask1 = ~((2**slotbits - 1) << b)
            table[a] = (table[a] & mask1) | (v << b)
        else:
            # b1 leftmost bits in table[a] = b1 rightmost bits of v, 
            b1 = 64 - b  # b1 leftmost bits in table[a]
            mask1 = (2**b) - 1  # only keep b rightmost bits
            v1 = (v & (2**b1 - 1))
            table[a] = (table[a] & mask1) | (v1 << b)
            # b2 rightmost bits in table[a+1] = b2 leftmost bits of v
            b2 = slotbits - b1
            mask2 = ~(2**b2 - 1)
            table[a+1] = (table[a+1] & mask2) | (v >> b1)


    @njit(locals=dict(i=int64, start=int64, page=int64),
          nogil=True, fastmath=True)
    def build_cumsizes(pss, css):
        i = 0
        start = 0
        for page in range(2**pagebits):
            if page % srate == 0:
                css[i] = start
                i += 1
            start += pss[page]


    # set shape (shp) and functions for fixed q.
    shp = tuple(range(q))
    revcomp_code, canonical_code = generate_revcomp_and_canonical_code(q)


    @njit(locals=dict(start=int64, end=int64, startpoints=int64,
            code=int64, c=int64, valid=int64, twofivefive=uint8, one=uint8),
          nogil=True, fastmath=True)
    def count_valid_qgrams_per_page(seq, start, end, pss):
        """
        Compute canonical code and page number of each q-gram.
        Increment pagesizes ('pss') according to how many times each page is hit.

        Return number of valid q-grams in seq[start:end],
        where a q-gram seq[i:i+q] in seq is valid if all elements are in 0,1,2,3.
        """
        # Note: pagesizes 'pss' should be initialized to zero before calling.
        # This function will just add to existing counters.
        twofivefive = 255
        one = 1
        valid = 0
        startpoints = (end - start) - shp[q-1]
        for i in range(start, start+startpoints):
            code = 0
            for j in shp:
                c = seq[i+j]
                if c > 3:
                    break
                code = (code << 2) + c
            else:  # code is valid, no break
                valid += 1
                code = canonical_code(code)
                page = get_page(code)
                if pss[page] < twofivefive:  pss[page] += one
        return valid


    @njit(locals=dict(start=int64, end=int64, genome=int64, retval=int64,
            startpoints=int64, i=int64, j=int64, code=int64, c=int64, result=int64), 
          nogil=True, fastmath=True)
    def hash_qgrams(seq, start, end, genome, table, pss, css):
        """
        Hash canonical codes of valid q-grams into hash table.
        Return (one) failing canonical code >= 0 on overflow.
        Return -1 on success.
        """
        retval = -1
        startpoints = (end - start) - shp[q-1]
        for i in range(start, start+startpoints):
            code = 0
            for j in shp:
                c = seq[i+j]
                if c > 3:
                    break
                code = (code << 2) + c
            else:  # code is valid, no break
                code = canonical_code(code)
                result = find_or_set_genome(table, pss, css, code, genome)
                if result < 0:
                    retval = code
        return retval  # success


    @njit(locals=dict(lcursor=int64, rcursor=int64, page=int64,
            psize=int64, asize=int64, s=int64, x=int64, g=int64, f=int64),
          nogil=True, fastmath=True)
    def compactify(table, pss, css):
        """
        Compactify hash table, and set new pagesizes 'pss' and cumsizes 'css'.
        Return number of items actually used.
        """
        lcursor = rcursor = 0
        for page in range(2**pagebits):
            psize = pss[page]  # current size of page
            asize = 0  # computes actual size of page
            for s in range(psize):
                x = get_item(table, rcursor)
                g = (x >> fprbits) & 3
                if g != 0:
                    asize += 1  # this slot is used
                    if lcursor < rcursor:
                        f = x & fprmask
                        set_item(table, lcursor, f, g)
                    lcursor += 1
                rcursor += 1
            pss[page] = asize
        build_cumsizes(pss, css)
        return lcursor


    @njit(locals=dict(nslots=int64, page=int64, size=int64, slot=int64, x=int64, g=int64),
          nogil=True, fastmath=True)
    def get_occupancy(table, pss):
        pagefill = np.zeros(256, dtype=np.int64)
        genomefill = np.zeros(4, dtype=np.int64)
        nslots = 0
        for page in range(2**pagebits):
            size = pss[page]
            pagefill[size] += 1
            nslots += size
        if table.size > 0:
            for slot in range(nslots):
                x = get_item(table, slot)
                g = (x >> fprbits) & 3
                genomefill[g] += 1
        return pagefill, genomefill


    return CompactingHash(
        hashtype="compacting",
        pagebits=pagebits,
        fprbits=fprbits,
        hashfunc=hashfunc,
        sbits=sbits,
        srate=srate,

        hashtable=hashtable,
        pagesizes=pagesizes,
        cumsizes=cumsizes,

        get_page=get_page,
        get_fingerprint=get_fingerprint,
        find_or_set_genome=find_or_set_genome,
        find_pagestart=find_pagestart,
        get_item=get_item,
        set_item=set_item,
        build_cumsizes=build_cumsizes,
        count_valid_qgrams_per_page=count_valid_qgrams_per_page,
        hash_qgrams=hash_qgrams,
        compactify=compactify,
        get_occupancy=get_occupancy,
        )
