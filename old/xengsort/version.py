VERSION = "0.1"
__version__ = VERSION

DESCRIPTION = """
xengsort is an application for sorting DNA reads from xenograft experiments.
"""
