"""
Factories for elementary hash functions
to split a k-mer code (2k bits) into page and fingerprint
get_page(code): return an integer with pagebits bits
get_fingerprint(code): return an integer with fprbits bits

Ideally, define the functions in such a way that there exists
a function g(page(code), fingerprint(code)) = code
for all 4**k codes.
"""

from math import ceil
from numba import njit, int64


def print_info(name, slotbits, pagesize, pagesizebits, pagebits, tablewords):
    print(name + ":")
    print("  slots: #bits:  {}".format(slotbits))
    print("  pages: #slots: {}, #bits: {}".format(pagesize, pagesizebits))
    print("  table: #pages: {}, #slots: {}, #bits: {} = {:.6f} GiB".format(
        2**pagebits, 2**pagebits*pagesize, tablewords*64, tablewords*8/10**9))


def generate_get_page_and_get_fingerprint(name, q, pagebits, fprbits=-1):
    # define get_page(code) function
    qbits = 2 * q
    if fprbits < 0:  fprbits = qbits - pagebits  # ensure no loss
    pagemask = 2**pagebits - 1
    fprmask = 2**fprbits - 1
    codemask = 2**qbits - 1

    if name == "lm3":
        # left bits (except first three) -(3)-++(pagebits)+++-----
        pageshift = qbits - (pagebits + 3)
        if pageshift < 0:
            raise ValueError("negative pageshift {}".format(pageshift))
        @njit(locals=dict(code=int64, hc=int64))
        def get_page(code):
            hc = (code >> pageshift) & pagemask
            return hc
        mask1 = 2**(qbits-pagebits-3) - 1
        mask2 = 7 * 2**(qbits-pagebits-3)
        @njit(locals=dict(code=int64, hc=int64))
        def get_fingerprint(code):
            hc = (code & mask1) | ((code >> pagebits) & mask2)
            return hc & fprmask

    elif name == "rm3":
        # right bits (except last three) ------++(pagebits)++---
        @njit(locals=dict(code=int64, hc=int64))
        def get_page(code):
            hc = (code >> 3) & pagemask
            return hc
        @njit(locals=dict(code=int64, hc=int64))
        def get_fingerprint(code):
            hc = code >> (pagebits+3)
            hc = (8*hc) | (code & 7)
            return hc & fprmask     

    elif name == "mid":
        # middle bits, ---+++(pagebits)+++--(right)--
        right = (qbits - pagebits) // 2
        rightmask = 2**right - 1
        @njit(locals=dict(code=int64, hc=int64))
        def get_page(code):
            hc = (code >> right) & pagemask
            return hc
        @njit(locals=dict(code=int64, hc=int64))
        def get_fingerprint(code):
            hc = code >> (pagebits+right)
            hc = ((2**right)*hc) | (code & rightmask)
            return hc & fprmask     
            
    elif name in ("swapslide", "final"):
        shift = qbits - pagebits
        @njit(locals=dict(code=int64, swap=int64, hc=int64))
        def get_page(code):
            swap = ((code << q) ^ (code >> q)) & codemask
            hc = swap ^ (swap >> shift)
            return hc & pagemask
        @njit(locals=dict(code=int64, swap=int64))
        def get_fingerprint(code):
            swap = ((code << q) ^ (code >> q))
            return swap & fprmask

    elif name == "slide":
        shift = qbits - pagebits
        @njit(locals=dict(code=int64, hc=int64))
        def get_page(code):
            hc = code ^ (code >> shift)
            return hc & pagemask
        @njit(locals=dict(code=int64, hc=int64))
        def get_fingerprint(code):
            return code & fprmask

    else:
        raise ValueError("name must be in {'lm3', 'rm3', 'mid', 'final', swapslide', 'slide'}.")
    return (get_page, get_fingerprint)
