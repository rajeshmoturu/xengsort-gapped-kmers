"""
Index two genomes with q-gram fingerprints,
using an engineered hash table.

Example: Index repeat-masked genome and transcriptome
(Download FASTA files from ENSEMBL):

xengsort index /vol/ref/xenograft/humo.h5  -k 25 -b 40 \
--graft /vol/ref/xenograft/Homo*  --host  /vol/ref/xenograft/Mus* \
--graftname human --hostname mouse

"""

from math import ceil
import datetime

import numpy as np
from numba import njit, int64
#from scipy.stats import poisson

from .dnaio import fasta_reads
from .dnaencode import dna_to_2bits
from .h5utils import save_to_h5group
from .cascadinghash import generate_cascading_hash
from .compactinghash import generate_compacting_hash

#################################################


def total_stored(pageocc):
    total = 0
    for occ in pageocc:
        total += sum([i*o for i, o in enumerate(occ)])
    return total


def analyze_occupancy(ch, pages=True, genomes=True):
    (occ, gocc) = ch.get_occupancy(ch.hashtable, ch.pagesizes)
    npages = np.sum(occ)
    nitems = 0
    if pages:
        print("page occupancy:")
        for (o, c) in enumerate(occ):
            if c != 0:
                print("  {:3d}: {:10d}".format(o, c))
                nitems += o*c
        print("({} pages total; {} items total)".format(npages, nitems))
        if occ[255] != 0 and not genomes:
            print("WARNING: overflows may occur on {} pages!".format(occ[255]))
    if genomes:
        print("genome occupancy:")
        for (g, c) in enumerate(gocc):
            print("  {:3d}: {:10d}".format(g, c))
        nitems = np.sum(gocc)
        ipp = nitems / npages if npages > 0 else 0.0
        print("({} items total; {:.3f} per page)".format(nitems, ipp))
    print()


## suggested indexing method
############################


def build_index(graftfasta, hostfasta, q, hashfunc, 
        pagebits, fprbits, samplingrate, samplebits):
    """
    Build a q-gram index based on a CompactingHash.
    graftfasta: list of graft FASTA files
    hostfasta:  list of host FASTA files
    q:          q-gram length == k-mer length
    hashfunc:   name of a hash function from generalhash.py
    pagebits:   bits to address a page; there are 2**pagebits pages,
                there should be about 5 times less pages than unique k-mers
    fprbits:    bits for a fingerprint, usually fprbits = 2*q - pagebits
    samplingrate: 1/fraction of pages for which the address is stored
    samplebits:   32 or 64, how to store page addresses, use 64 for large datasets
    """
    print("build_index: using {}-mers with parameters {}, {}, {}".format(q, hashfunc, pagebits, fprbits))    
    npages = 2**pagebits
    print("using {:.3f} M pages\n".format(npages/10**6))
    _KIND = ['none', 'graft', 'host']  # 1: graft, 2: host

    # first pass
    print("pass 1 starting at {:%Y-%m-%d %H:%M:%S}".format(datetime.datetime.now()))
    print("pass 1: determine page occupancy")
    ch = generate_compacting_hash(q, pagebits, fprbits, hashfunc, samplebits, samplingrate)
    count_valid_qgrams_per_page = ch.count_valid_qgrams_per_page
    pagesizes = ch.pagesizes
    valid = 0
    for (fastas, genome) in zip((graftfasta,hostfasta), (1,2)):
        for fasta in fastas:
            print("processing {}: {}...".format(_KIND[genome], fasta))
            for header, seq in fasta_reads(fasta):
                sq = dna_to_2bits(seq)
                dvalid = count_valid_qgrams_per_page(sq, 0, len(sq), pagesizes)
                valid += dvalid
    print("valid q-grams:", valid)
    analyze_occupancy(ch, pages=True, genomes=False)
    print("pass 1 finished at {:%Y-%m-%d %H:%M:%S}".format(datetime.datetime.now()))
    
    # second pass: re-generate the hash structure with the table
    print("pass 2 starting at {:%Y-%m-%d %H:%M:%S}".format(datetime.datetime.now()))
    print("pass 2: hash {}-mers".format(q))
    failed = False
    ch = generate_compacting_hash(q, pagebits, fprbits, hashfunc, samplebits, samplingrate, pagesizes)
    ch.build_cumsizes(ch.pagesizes, ch.cumsizes)
    hash_qgrams = ch.hash_qgrams
    table, pagesizes, cumsizes = ch.hashtable, ch.pagesizes, ch.cumsizes
    for (fastas, genome) in zip((graftfasta,hostfasta), (1,2)):
        for fasta in fastas:
            print("processing {}: {}...".format(_KIND[genome], fasta))
            for header, seq in fasta_reads(fasta):
                sq = dna_to_2bits(seq)
                errorcode = hash_qgrams(sq, 0, len(sq), genome, table, pagesizes, cumsizes)
                if errorcode >= 0:
                    print("ERROR: overflow on canonical code {}".format(errorcode))
                    failed = True

    # compact and clean up
    analyze_occupancy(ch, pages=False, genomes=True)
    print("compacting index at{:%Y-%m-%d %H:%M:%S}".format(datetime.datetime.now()))    
    unique = ch.compactify(table, pagesizes, cumsizes)
    print("unique {}-mers: {}".format(q, unique))
    analyze_occupancy(ch, pages=True, genomes=True)
    return (ch, not failed)

    
def main(args):
    """main method for indexing"""
    print("starting at {:%Y-%m-%d %H:%M:%S}".format(datetime.datetime.now()))
    k = args.kmer_size
    pagebits = args.pagebits
    fprbits = args.fingerprintbits
    if fprbits < 0:
        fprbits = 2*k - pagebits
    (index, success) = build_index(args.graft, args.host, k, 
        args.hashfunction, pagebits, fprbits, args.samplerate, args.samplebits)
    # save to args.index, store args.graftname, args.hostname as well
    print("SUCCESS!" if success else "FAILED!")
    print("saving to disk at {:%Y-%m-%d %H:%M:%S}".format(datetime.datetime.now()))


# alternative index construction
# ##############################


def parse_parameters_alternative(params, q):
    # parse args.parameters
    lpars = [x.strip() for x in params.split(";")]
    result = list()
    for lp in lpars:
        tfpsr = [x.strip() for x in lp.split(',')]
        t = tfpsr[0]
        f = [x.strip() for x in tfpsr[1].split(':')]
        f = f[0] if len(f)==1 else tuple(f)
        p = int(tfpsr[2])
        s = int(tfpsr[3])
        r = -1 if len(tfpsr) < 5 else int(tfpsr[4])
        if r < 0:           
            r = 2*q if t =="final" else 2*q - p
        result.append((t,f,p,s,r))
    return result


def save_index_alternative(outfile, ch, description):
    levels = description['levels']
    save_to_h5group(outfile, 'info', **description)
    for i in range(levels):
        group = 'level'+str(i)
        save_to_h5group(outfile, group, hashtable=ch.hashtables[i])


def build_index_alternative(graftfasta, hostfasta, q, parameters):
    """
    Build a q-gram index based on a CascadingHash with different components.
    graftfasta: list of graft FASTA files
    hostfasta:  list of host FASTA files
    q:          q-gram length == k-mer length
    parameters: parsed parameters
    """
    print("building an index from {}-mers...".format(q))
    levels = len(parameters)
    print("building a {}-level hash structure with the following parameters:".format(levels))
    for i,p in enumerate(parameters):
        print("level {}: {}".format(i, p))
    hashtypes = tuple(p[0] for p in parameters)  # strings, e.g. 'simple'
    hashfuncs = tuple(p[1] for p in parameters)  # strings or pairs
    pagebits = tuple(p[2] for p in parameters)  # ints
    pagesizes = tuple(p[3] for p in parameters) # ints
    fprbits = tuple(p[4] for p in parameters)  # ints

    overflowsize = 300000000
    print("allocating buffers; emergency overflowsize = {}".format(overflowsize))
    ch = generate_cascading_hash(q, hashtypes, hashfuncs, pagebits, pagesizes, fprbits, overflowsize)
    totalmemory = sum( ceil(chi.memorybits/64) for chi in ch.components) * 8
    print("total hash table memory: {:.6f} GiB".format(totalmemory/10**9))
    hash_qgrams = ch.hash_qgrams
    ht = ch.hashtables
    ot = ch.overflow

    over = valid = 0
    _KIND = ['none', 'graft', 'host']  # 1: graft, 2: host
    for (fastas, genome) in zip((graftfasta,hostfasta), (1,2)):
        for fasta in fastas:
            print("processing {}: {}...".format(_KIND[genome], fasta))
            for header, seq in fasta_reads(fasta):
                sq = dna_to_2bits(seq)
                dvalid, over = hash_qgrams(sq, 0, len(sq), genome, ht, ot, over)
                valid += dvalid
    print("valid q-grams:", valid)
    print("upper bound on overflows:", over)
    pageocc, genomeocc = ch.get_occupancy(ht, ot[:over])
    #print("page occupancy (0, 1, ...):", pageocc, sep="\n")
    total = over + total_stored(pageocc)
    bpkmer = totalmemory / total
    print("upper bound on total q-grams:", total)
    print("overall space: {:.4f} bytes/{}mer".format(bpkmer, q))
    print("genome occupancy:", genomeocc, sep="\n")
    for lvl, occ in enumerate(pageocc):
        print("analyzing occupancy at level {}: {}".format(lvl, occ))
        nbytes = ht[lvl].size * 8
        total = analyze_occupancy_alternative(occ, nbytes, total)

    description = dict(
        q=q, levels=len(hashtypes),
        hashtypes=hashtypes, hashfuncs=hashfuncs,
        pagebits=pagebits, pagesizes=pagesizes, fprbits=fprbits)
    return ch, description, over


def analyze_occupancy_alternative(occ, nbytes, valid):
    maxsize = len(occ) - 1  # pagesize from 1..maxsize
    numpages = sum(occ)
    for pagesize in range(1, maxsize+1):
        stored = sum([min(i,pagesize)*o for i,o in enumerate(occ)])
        remainder = valid - stored
        mem = nbytes * pagesize / maxsize
        wasted = 1.0 - stored/(pagesize*numpages)
        wastedmem = wasted * mem
        surplus = (pagesize*numpages)/stored - 1.0
        print("    pagesize {}: bytes/qgram: {:.3f};  stored: {:.2%};  remainder: {};  surplus: {:.2%};  empty: {:.3f} GiB".format(
            pagesize, mem/stored, stored/valid, remainder, surplus, wastedmem/10**9))
    return remainder


def main_alternative(args):
    """main method for indexing"""
    ## xenoindex  graho.h5 --graft graft.fa --host host.fa
    print("starting at {:%Y-%m-%d %H:%M:%S}".format(datetime.datetime.now()))
    k = args.kmer_size
    parameters = parse_parameters_alternative(args.parameters, k)
    ch, desc, over = build_index_alternative(args.graft, args.host, k, parameters)

    outname = args.index
    if (outname is not None) and (over == 0):
        print("saving to '{}' at {:%Y-%m-%d %H:%M:%S}".format(outname, datetime.datetime.now()))
        desc['graftname'] = args.graftname
        desc['hostname'] = args.hostname
        save_index_alternative(outname, ch, desc)
    else:
        print("not saving; outfile={}; overflow={}".format(outname, over))
    print("ending at {:%Y-%m-%d %H:%M:%S}".format(datetime.datetime.now()))
