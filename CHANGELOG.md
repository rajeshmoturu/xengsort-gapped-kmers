# Changelog

### 1.1.0
* Fixed a bug/inconsistency that was introduced when the hdf5/h5py libraries changed their default behavior decoding strings vs. bytes. It could happen that the rcmode parameter of the stored hash table (how to deal with reverse complements) would be interpreted in a wrong manner; this could lead to inconsistent results. We strongly recommend to update to ensure that this bug does not affect your results.

### 1.0.3
* Change description and README because of `easy_install` deprecation

### 1.0.2
* Make `--out` a require parameter for `xengsort classify`.
* Improve the documentation, especially the troubleshooting section.

### 1.0.1
* Better error messages when erroneously giving gzipped FASTQ files to `xengsort classify` instead of uncompressed FASTQ files.
* Make `-n` a required parameter for xengsort index.

### 1.0.0
* Initial public release.
